function verificar(){
    var data = new Date
    var ano = data.getFullYear()
    var fAno = document.getElementById("txtano")
    var res = document.getElementById("res")
    if(fAno.value.length == 0 || fAno.value > ano){
        window.alert("[ERRO] Verifique os dados e tente novamente! ")
    } else{
        var fsex = document.getElementsByName("radsex")
        var idade = ano - Number(fAno.value)
        res.innerHTML  = `Idade calculada: ${idade}`
        var  genero = ''
        var img = document.createElement('img')
        img.setAttribute('id', 'foto')
        if(fsex[0].checked){
            genero = "Homem"
            if(idade >=0 && idade <10){
                img.setAttribute('src', 'hcrianca.png')
            }
            else if(idade < 21){
                img.setAttribute('src', 'hjovem.png')
            }
            else if(idade < 50){
                img.setAttribute('src', 'hadulto.png')
            }
            else{
                img.setAttribute('src', 'hvelho.png')
            }
        }
        else if(fsex[1].checked){
            genero = 'Mulher'
            if(idade >=0 && idade <10){
                img.setAttribute('src', 'mcrianca.png')
            }
            else if(idade < 21){
                img.setAttribute('src', 'mjovem.png')
            }
            else if(idade < 50){
                img.setAttribute('src', 'madulta.png')
            }
            else{
                img.setAttribute('src', 'mvelha.png')
            }
        }
        res.style.textAlign = 'center'
        res.innerHTML = `Detectamos ${genero} com ${idade} anos`
        res.appendChild(img)
    }
}
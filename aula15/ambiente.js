/*
let num  = [4, 3, 2, 10, 40, 20]

console.log(num)
console.log(`O vetor tem ${num.length} posicoes`)
console.log(`O primeiro Valor do vetor é ${num[0]}`)
num.sort()
for(let pos in num){
    console.log(num[pos])
}

console.log(num.indexOf(11))
*/

var a = [44, 7, 5, 6, 4, 2, 1];
a.sort() // função sort pode dar problemas caso nao inplemente uma funçao para ela
/* quando não é passada uma função o que é feito fica à vontade do browser. 
Na MDN refere-se que os valores serão tratados como String e acho que é isso que a 
maior pare dos browsers faz. (ler mais na MDN em Inglês aqui) 
*/
a.sort(function(a, b) { return a - b; });
console.log(a)
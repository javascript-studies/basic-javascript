let numeros = []
let numero = document.querySelector("input#txtnun")
var lista = document.getElementById("lista")
let resultado = document.querySelector("div#res")

function isNum(n){
    n = Number(n)
    if(n<1  || n> 100) return false
    else return true 
}

function inList(n, l){
    if(l.indexOf(Number(n)) != -1) return true
    else return false
}

function AdicionarNumero(){
    if(numero.value.length !=0 && isNum(numero.value) && !inList(numero.value, numeros) ){
        let item = document.createElement("option")
        numeros.push(Number(numero.value))
        item.text = ` Valor ${numero.value} adicionado`
        lista.append(item)
        res.innerHTML = ""
    }
    else{
        alert("[ERROR] Valor inválido ou ja encontrado na lista")   
    }
    numero.value= ''
    numero.focus()
}

function AnalizarNumeros(){
    if(numeros.length==0){
        alert("[ERRO] não há números para analizar")
    }
    else{
        resultado.innerHTML= ""
        let quantidade_numeros = numeros.length

        let maior = numeros[0]
        let menor = numeros[0]
        let soma = 0
        for(let pos in numeros){
            soma+= numeros[pos]
            
            if(numeros[pos] > maior){
                maior = numeros[pos]
            }
            if(numeros[pos]< menor){
                menor = numeros[pos]
            }
        }
        
        let media = soma/quantidade_numeros

        resultado.innerHTML+= `<p> A quantidade de números sao ${quantidade_numeros}</p>`
        resultado.innerHTML+= `<p> O maior número é ${maior}</p>`
        resultado.innerHTML+= `<p> O menor número é ${menor}</p>`
        resultado.innerHTML+= `<p> A soma dos números é ${soma}</p>`
        resultado.innerHTML+= `<p> A média dos números é ${media}</p>`
    }

}


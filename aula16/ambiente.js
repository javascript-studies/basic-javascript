// ensinando a utilizar funçoes em js oq é bem parecido com a linguagem c

function soma(n1=0, n2= 0) {
// Em JS se algum dos parâmetros nao for passado voce pode deixar pressetado algum valor para impedir que de algum erro
    return n1+n2
}
console.log(soma(2))
console.log(soma(4, 10))

// Voce pode tambem jogar uma funçao dentro de uma variável

let v = function(x){
    return x*2
}
console.log("PASSANDO UMA FUNÇAO PARA UMA VARIAVEL")
console.log(v(3))


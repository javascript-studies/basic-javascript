function Contar(){
    var inicio = document.getElementById("txtinicio")
    var fim = document.getElementById("txtfim")
    var passo = document.getElementById("txtpasso")
    var res = document.getElementById("res")

    if(inicio.value.length ==0 ||  fim.value.length == 0 || passo.value.length == 0){
        window.alert("[ERRO] Faltam dados")
        res.innerHTML = "Impossivel contar..."
        
    } else {
        inicio = Number(inicio.value)
        fim = Number(fim.value)
        passo = Number(passo.value)

        if(inicio < 0 || inicio > fim || passo == 0 || passo> fim-inicio ){
            alert("[Erro] Valores inválidos. Por favor digite valores validos")
        }else {
            res.innerHTML = "Contando:<br> "
            for(inicio; inicio<=fim; inicio+=passo){
                res.innerHTML += ` ${inicio} \u{1F608} `
            }
        }

    }
}
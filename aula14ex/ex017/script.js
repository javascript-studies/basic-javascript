function Tabuada(){
    let nun = document.getElementById("txtnun")
    let tab = document.getElementById("seltab")
    if(nun.value.length !=0){
        let n = Number(nun.value)
        let i = 1
        tab.innerHTML = ""
        while(i <=10){
            //alert(`entrou no while ${i} vezes`)
            let item = document.createElement("option")
            item.text = ` ${n} x ${i} = ${n*i}`
            item.value = `tab${i}`
            tab.append(item)
            i++
        }
    }else{
        window.alert("Por favor digite um número")
    }

}